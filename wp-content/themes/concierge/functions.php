<?php
/**
* @package WordPress
* @subpackage concierge
* @since 1.0
*/



/*-------------------------------------------------------------------------
  START INITIALIZE FILE LINK
------------------------------------------------------------------------- */

require_once(TEMPLATEPATH . '/framework/functions.php');

/*-------------------------------------------------------------------------
  END INITIALIZE FILE LINK
------------------------------------------------------------------------- */




/*-------------------------------------------------------------------------
  START AJAXURL AS GLOBAL VARIBALE
------------------------------------------------------------------------- */

function concierge_ajaxurl() { 

  if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    global $woocommerce;
    $checkout_url = $woocommerce->cart->get_checkout_url();
  }  

 ?>

    <script type="text/javascript">
        var ajaxurl = '<?php echo esc_js(admin_url('admin-ajax.php')); ?>';
        var current_page = '<?php echo esc_js(get_post_meta( get_the_ID(), '_wp_page_template', true )); ?>';
        var checkout_page_url = '<?php if(isset($checkout_url)){ echo $checkout_url;} ?>'; 
    </script>

<?php

 }

add_action('wp_head','concierge_ajaxurl');

/*-------------------------------------------------------------------------
  END AJAXURL AS GLOBAL VARIBALE
------------------------------------------------------------------------- */



/*-------------------------------------------------------------------------
  START WP TITLE FILTER
------------------------------------------------------------------------- */

function concierge_wp_title( $title, $sep ) {
  global $paged, $page;

  if ( is_feed() ) {
    return $title;
  }

  // Add the site name.
  $title .= get_bloginfo( 'name', 'display' );

  // Add the site description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) ) {
    $title = "$title $sep $site_description";
  }

  // Add a page number if necessary.
  if ( $paged >= 2 || $page >= 2 ) {
    $title = "$title $sep " . sprintf( __( 'Page %s', 'concierge' ), max( $paged, $page ) );
  }

  return $title;
}
add_filter( 'wp_title', 'concierge_wp_title', 10, 2 );

/*-------------------------------------------------------------------------
  END WP TITLE FILTER
------------------------------------------------------------------------- */


/*-------------------------------------------------------------------------
  START EXCERPT LENGTH
------------------------------------------------------------------------- */


function concierge_custom_excerpt_length( $length ) {
  return 37;
}
add_filter( 'excerpt_length', 'concierge_custom_excerpt_length', 999 );


/*-------------------------------------------------------------------------
  END EXCERPT LENGTH
------------------------------------------------------------------------- */


/*-------------------------------------------------------------------------
  Skip Add to Cart to Check Out
------------------------------------------------------------------------- */
/*
function my_custom_add_to_cart_redirect( $url ) {
	$url = WC()->cart->get_checkout_url();
	// $url = wc_get_checkout_url(); // since WC 2.5.0
	return $url;
}
add_filter( 'woocommerce_add_to_cart_redirect', 'my_custom_add_to_cart_redirect' );
*/
/*-------------------------------------------------------------------------
  Display only Vechicle types - forsale Category on Shop page
------------------------------------------------------------------------- */
add_action('pre_get_posts','shop_filter_cat');

 function shop_filter_cat($query) {
    if (!is_admin() && is_post_type_archive( 'product' ) && $query->is_main_query()) {
       $query->set('tax_query', array(
                    array ('taxonomy' => 'vechicle_type',
                                       'field' => 'slug',
                                        'terms' => 'jets-for-sale'
                                 )
                     )
       );   
    }
 }
/*-------------------------------------------------------------------------
  owl.carousel slider
------------------------------------------------------------------------- */
// helper function to return first regex match
function get_match( $regex, $content ) {
    preg_match($regex, $content, $matches);
    return $matches[1];
} 

// Extract the shortcode arguments from the $page or $post
	$shortcode_args = shortcode_parse_atts(get_match('/\[gallery\s(.*)\]/isU', $post->post_content));

	// get the ids specified in the shortcode call
	$ids = $shortcode_args["ids"];
	
usort($attachments, 'sort_menu_order');

function sort_menu_order($a, $b) {
    // change the direction of the > to toggle asc/desc
    return $a->menu_order > $b->menu_order;
}
function add_flexslider($post_id) {
    global $post;
	$thumbnail_ID = get_post_thumbnail_id();
	
    $order = 'DESC';
	$orderby = 'post__in';
    $include = $attr['ids'];
	
     $images = get_posts(array('include' => $include, 'numberposts' => -1, 'post_parent' => get_the_ID(), 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ));
     
	 if ($images) :
	 foreach ( $images as $key => $val ) {
        $images[$val->ID] = $images[$key];
    }
		echo '<div class="flexslider">';
        echo '<ul class="slides">';
         foreach ($images as $attachment_id => $image) :
		 			 
             $img_alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true); //alt
             if ($img_alt == '') : $img_alt = $image->post_title; endif;
 
             $big_array = image_downsize( $image->ID, 'shop_single' );
             $img_url = $big_array[0];
 
             echo '<li>';
             echo '<img src="';
             echo $img_url;
             echo '" alt="';
             echo $img_alt;
             echo '" />';
             echo '</li><!--end slide-->';
 
     endforeach; endif; 
	 echo '</ul>';
echo '</div>';}



/*-------------------------------------------------------------------------
  Enqueue Scripts
------------------------------------------------------------------------- */
function rudr_css_and_js_for_slider() {
	wp_enqueue_style( 'flexslider', get_stylesheet_directory_uri() . '/assets/css/flexslider.css', '', null );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'flexsliderjs', get_stylesheet_directory_uri() . '/assets/js/jquery.flexslider.js', array('jquery'), null, true );
	wp_enqueue_script( 'Gmaps', 'http://maps.google.com/maps/api/js?key=AIzaSyAxYDb0YAKKUozzUi5NvuDHKvmAyw2iBqE&libraries=geometry', array(), false, true );    
	wp_enqueue_script( 'gmaps_scripts',  get_bloginfo( 'template_directory' ) . '/assets/js/gmaps.js', array( 'Gmaps' ), '1.0', true );
        wp_enqueue_script( 'hmac-sha256', get_stylesheet_directory_uri() . '/assets/js/sha256.js' ); 
        
    
	} 
add_action( 'wp_enqueue_scripts', 'rudr_css_and_js_for_slider' );

/*-------------------------------------------------------------------------
  Post Booking Form to Admin Email Address
------------------------------------------------------------------------- */
add_action("wp_ajax_concierge_booking_data", 'concierge_booking_data' );
add_action("wp_ajax_nopriv_concierge_booking_data", "concierge_booking_data");

function concierge_booking_data(){
	if(!empty($_POST)) {

  $error = "";

  //Check to make sure sure that a valid email address is submitted
  if(trim($_POST['email']) == '')  {
    $error .= "An email address is required<br/>";
  } else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {
    $error .= "A valid email address is required<br/>";
  } else {
    $email = trim($_POST['email']);
  }

  //Check to make sure that the first name field is not empty
  if(trim($_POST['name']) == '') {
    $error .= "Your name is required<br/>";
  } else {
    $name = trim($_POST['name']);
  }

  //Check to make sure that the last name field is not empty
  if(trim($_POST['phone']) == '') {
    $error .= "Your phone number is required<br/>";
  } else {
    $phone = trim($_POST['phone']);
  }

  if(trim($_POST['company']) == '') {
    $company = "They had no company name at this time";
  } else {
    $company = $_POST['company'];
  }
  
  if(trim($_POST['person_select']) == '') {
    $person_select = "No more person selected at this time";
  } else {
    $person_select = $_POST['person_select'];
  }
  
  if(trim($_POST['check_in']) == '') {
    $check_in = "No depart date at this time";
  } else {
    $check_in = $_POST['check_in'];
  }
  
  if(trim($_POST['departuretime']) == '') {
    $departuretime = "No depart time at this time";
  } else {
    $departuretime = $_POST['departuretime'];
  }  
  
  if(trim($_POST['departure']) == '') {
    $departure = "No return date at this time";
  } else {
    $departure = $_POST['departure'];
  }
  
  if(trim($_POST['returntime']) == '') {
    $returntime = "No return time at this time";
  } else {
    $returntime = $_POST['returntime'];
  }

  if(trim($_POST['message']) == '') {
    $message = "They had no departure at this time";
  } else {
    $message = $_POST['message'];
  }
	

  $admin_mail = get_bloginfo('admin_email');
	

if(empty($error)) {

  $message  = "New Booking details. \n\n
               Name: " . $name . "\n
               Email: " . $email . "\n
			   Phone: " . $phone . "\n
               Company: " . $company . "\n
			   Depart Date: " . $check_in . "\n
			   Depart Time: " . $departuretime . "\n
			   Return Date: " . $departure . "\n
			   Return Time: " . $returntime . "\n
			   Persons: " . $person_select . "\n			   
			   Notes: " . $message . "\n";

			   

  $jvMail = wp_mail($admin_mail, 'Fleet Booking from your website', $message);
  echo "sent";

} else {
  echo $error;
}
die();
}
}
/*-------------------------------------------------------------------------
  Change breadcrumb on Shop page
------------------------------------------------------------------------- */
