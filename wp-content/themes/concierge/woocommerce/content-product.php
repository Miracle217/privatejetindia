<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array('col-lg-4', 'col-md-6',' col-sm-12',' fleet-grid', 'layout-grid','item');
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
?>





<div <?php post_class( $classes ); ?> >

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<div class="thumb fleet-thumb">
								
					
		<div class="overlay">				
		
        <?php add_flexslider("$post_id"); ?>						
		
		</div>
	</div>

	<?php $product_meta = get_post_custom($post->ID);  ?>

	<div class="fleet-vechicle-content">

		<header class="fleet-vechicle-header">

			<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
			
		</header>


  
  
        
  <div class="tree">  
<p align="center">  
  <button id="all" style="margin-left:auto;margin-right:auto;display:block;">Show properties</button>
  </p>
        <ul class="custom-list fleet-vechicle-properties">
			<li><a>....</a>
			<ul>
			<?php if(isset($product_meta['_concierge_vehicle_max_passenger'][0]) && !empty($product_meta['_concierge_vehicle_max_passenger'][0])): ?>
			<li><a><span><?php _e('Max Seats','concierge') ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_max_passenger'][0]); ?></strong></a></li>
			<?php endif; ?>

			<?php if(isset($product_meta['_concierge_vehicle_age'][0]) && !empty($product_meta['_concierge_vehicle_age'][0])): ?>
			<li><a><span><?php _e('Vehicle Age ','concierge') ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_age'][0]); ?></strong></a></li>
			<?php endif; ?>

			
			<?php if(isset($product_meta['_concierge_vehicle_people_capacity'][0]) && !empty($product_meta['_concierge_vehicle_people_capacity'][0])): ?>
			<li><a><span><?php _e('Beds','concierge'); ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_people_capacity'][0]); ?><?php if(!empty($product_meta['_concierge_vehicle_additional_people_capacity'][0])): ?>+<?php echo esc_attr($product_meta['_concierge_vehicle_additional_people_capacity'][0]); ?><?php endif; ?></strong></a> </li>
			<?php endif; ?>

			<?php if(isset($product_meta['_concierge_vehicle_max_speed'][0]) && !empty($product_meta['_concierge_vehicle_max_speed'][0])): ?>
			<li><a><span><?php _e('Max Range','concierge'); ?> <strong><?php echo esc_attr($product_meta['_concierge_vehicle_max_speed'][0]); ?></strong></a> </li>
			<?php endif; ?>

			<?php if(isset($product_meta['_concierge_vehicle_fuel_capacity'][0]) && !empty($product_meta['_concierge_vehicle_fuel_capacity'][0])): ?>
			<li><a><span><?php _e('Max Hrs/Flight','concierge'); ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_fuel_capacity'][0]); ?></strong></a></li>
			<?php endif; ?>

			<?php if(isset($product_meta['_concierge_vehicle_max_weight'][0]) && !empty($product_meta['_concierge_vehicle_max_weight'][0])): ?>
			<li><a><span><?php _e('Cruise Speed','concierge'); ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_max_weight'][0]); ?></strong></a></li>
			<?php endif; ?>

			<?php if(isset($product_meta['_concierge_vehicle_baggage_capacity'][0]) && !empty($product_meta['_concierge_vehicle_baggage_capacity'][0])): ?>
			<li><a><span><?php _e('Baggage Capacity','concierge'); ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_baggage_capacity'][0]); ?></strong></a></li>
			<?php endif; ?>
			
			<?php if(isset($product_meta['_concierge_vehicle_registration_no'][0]) && !empty($product_meta['_concierge_vehicle_registration_no'][0])): ?>
			<li><a><span><?php _e('Registration No.','concierge'); ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_registration_no'][0]); ?></strong></a></li>
			<?php endif; ?>
			
			<?php if(isset($product_meta['_concierge_vehicle_total_time'][0]) && !empty($product_meta['_concierge_vehicle_total_time'][0])): ?>
			<li><a><span><?php _e('Total Time','concierge'); ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_total_time'][0]); ?></strong></a></li>
			<?php endif; ?>
			
			<?php if(isset($product_meta['_concierge_vehicle_jet_location'][0]) && !empty($product_meta['_concierge_vehicle_jet_location'][0])): ?>
			<li><a><span><?php _e('Location','concierge'); ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_jet_location'][0]); ?></strong></a></li>
			<?php endif; ?>
			
			<?php if(isset($product_meta['_concierge_vehicle_serial_no'][0]) && !empty($product_meta['_concierge_vehicle_serial_no'][0])): ?>
			<li><a><span><?php _e('Serial No.','concierge'); ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_serial_no'][0]); ?></strong></a></li>
			<?php endif; ?>
			
			<?php if(isset($product_meta['_concierge_vehicle_jet_year'][0]) && !empty($product_meta['_concierge_vehicle_jet_year'][0])): ?>
			<li><a><span><?php _e('Year','concierge'); ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_jet_year'][0]); ?></strong></a></li>
			<?php endif; ?>
			
			<?php if(isset($product_meta['_concierge_vehicle_total_cycle'][0]) && !empty($product_meta['_concierge_vehicle_total_cycle'][0])): ?>
			<li><a><span><?php _e('Total Cycle','concierge'); ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_total_cycle'][0]); ?></strong></a></li>
			<?php endif; ?>
			
			<?php if(isset($product_meta['_concierge_vehicle_program_coverage'][0]) && !empty($product_meta['_concierge_vehicle_program_coverage'][0])): ?>
			<li><a><span><?php _e('Program Coverage','concierge'); ?><strong><?php echo esc_attr($product_meta['_concierge_vehicle_program_coverage'][0]); ?></strong></a></li>
			<?php endif; ?>
			</ul>
			</li>
		</ul>
	</div>	


		
	</div>

	<?php //do_action( 'woocommerce_after_shop_loop_item' ); ?>

</div>





