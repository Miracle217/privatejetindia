<?php

global $woocommerce, $product;

?>

	<!--<!?php do_action( 'woocommerce_before_add_to_cart_form' ); ?> id="booking-form" `-->
	
	
	<form id="send_booking_to_office" type="post" action="" class="uou-form default-form" enctype='multipart/form-data'>
		
		<?php $get_product_base_cost = get_post_meta($product->id, 'uou_bookable_main_cost', true ); ?>

		<div id="uou-bookings-booking-form">
			
		<!-- Start Name Field-->
		<p class="form-row">
			<span title="Name">
				<input type="text" name="name" placeholder="<?php _e('Name', 'uou-bookings') ?>">
			</span>
		</p>
		<!-- End Name Field -->
		
		<!-- Start Email Field-->
		<p class="form-row">
			<span title="Email">
				<input type="text" name="email" placeholder="<?php _e('Email', 'uou-bookings') ?>">
			</span>
		</p>
		<!-- End Email Field-->
		
		<!-- Start Phone Field-->
		<p class="form-row">
			<span title="Phone">
				<input type="text" name="phone" placeholder="<?php _e('Phone', 'uou-bookings') ?>">
			</span>
		</p>
		<!-- End Phone Field-->
		
		<!-- Start Company Field-->
		<p class="form-row">
			<span title="Company">
				<input type="text" name="company" placeholder="<?php _e('Company', 'uou-bookings') ?>">
			</span>
		</p>
		<!-- End Company Field-->
		
		<h5><?php _e('Itinerary:', 'uou-bookings') ?></h5>
		<!--<select class="my_select_box" multiple data-placeholder="Select Your Options" id="resoruce_select" name="resoruce_select">
		<!?php

			$booking_resource_meta = json_decode(get_post_meta( $product->id,'bookable_availibility_resource',true));

		    $resource_name = '';
		    $resource_value = '';
		    
		    if(isset($booking_resource_meta) && !empty($booking_resource_meta)){

		    	foreach ($booking_resource_meta as $meta) {

			    	foreach ($meta as $key => $value) {
			    		if($meta[$key]->name == 'uou_booking_resouce_availibility'){
			    			$resource_name = $meta[$key]->value;

			    			$post_7 = get_post($resource_name, ARRAY_A);
							$title = $post_7['post_title'];
			    		}
			    		if($meta[$key]->name == '_uou_booking_resource_cost'){
			    			$resource_value = $meta[$key]->value;
			    		}
			    	}
			    	?>

			        	<!?php $val = array("title","resource_value"); ?>
					    <option value='{"resource_name":"<!?php echo esc_attr($title); ?>","cost":"<!?php echo esc_attr($resource_value); ?>"}'><!?php echo esc_attr($title) . ' : &nbsp; &nbsp; ' . esc_attr(get_woocommerce_currency_symbol()).$resource_value; ?></option>


			        <!?php
			    }

		    }
		    

		?>-->

	</select><!-- #end resource -->		
		
		<p><?php _e('Departure Date', 'uou-bookings') ?></p>			
			<p id="check-in-wrapper" class="calendar-input">
				<span class="calendar-input input-left" title="Depart On">
					<input type="text" class="check-in" name="check_in" id="arrival" name="arrival" placeholder="<?php _e('Depart On', 'uou-bookings') ?>">
					<i class="fa fa-calendar check-in"></i>
				</span>
			</p>
			
			<!-- Start Depart Time Field-->
			<p><?php _e('Departure Time', 'uou-bookings') ?></p>
			<p class="form-row">
				<span title="Depart Time">
					<input type="text" id="departure_time" name="departuretime" placeholder="<?php _e('Depart Time', 'uou-bookings') ?>">
				</span>
			</p>
			<!-- End Depart Time Field-->
			
			<p style="margin-top:5px; margin-bottom:-10px;"><?php _e('Return', 'uou-bookings') ?></p>
			<span id="testcheckbox" class="checkbox-input">
				<p style="color:#000;">yes/no  <input id="testcheckbox" type="checkbox" name="checkbox"></p>
			</span>	

	
			
			<p><?php _e('Return Date', 'uou-bookings') ?></p>
			<p id="check-out-wrapper" class="calendar-input">
				<span class="calendar-input input-left" title="Return On">
					<input type="text" class="check-out" id="departure" name="departure" value="" placeholder="<?php _e('Return On', 'uou-bookings') ?>" disabled="disabled">
					<i class="fa fa-calendar check-out"></i>
				</span>
			</p>
			
			<!-- Start Return Time Field-->
			<p><?php _e('Return Time', 'uou-bookings') ?></p>
			<p class="form-row">
				<span title="Return Time">
					<input type="text" class="return_timeoff" id="return_time" name="returntime" placeholder="<?php _e('Return Time', 'uou-bookings') ?>" disabled="disabled">
				</span>
			</p>
			<!-- End Return Time Field-->
			
			<!-- Start No. of Persons Field-->
			<p><?php _e('No. of Persons', 'uou-bookings') ?></p>
			<p class="form-row">
				<span title="No. of Persons">
					<input type="text" id="person_select" name="person_select" placeholder="<?php _e('No. of Person', 'uou-bookings') ?>">
				</span>
			</p>
			<!-- End No. of Persons Field-->
			
			
			<p><label> <?php _e('Notes', 'uou-bookings') ?></label></p>
			<p class="form-row">	
			<span class="form-row" title="Notes">			
				<textarea name="message" placeholder="<?php _e('Notes', 'uou-bookings') ?>"></textarea>
			</span>
			</p>
			
 			<!--<!?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>-->

		</div>

		<h5 class="calculate-cart-total"><?php _e('Total Booking Cost : &nbsp;', 'uou-bookings') ?><?php echo esc_attr(get_woocommerce_currency_symbol()); ?><span class = "total_cost"></span></h5>

		
		<input type="hidden" name="action" value="concierge_booking_data" />
		<button class="button" type="submit">BOOK NOW</button>
		<!--<!?php do_action( 'woocommerce_after_add_to_cart_button' ); ?> -->
		<div class="send-booking-success">
			<h5 class="success_info"> <?php _e( 'Message send successfully !', 'concierge' ); ?></h5>
		</div>  
		  
	
	<!--<!?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>-->
