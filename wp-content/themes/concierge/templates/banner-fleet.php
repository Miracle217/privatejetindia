<style>
.banner-search .tab-title {
  display: none;
}
.banner-search .submit-btn {
  border-left: none;
}
.banner-search-inner {  padding: 0 0; }
.banner-search .tab-title:hover { display: none; }

.banner-search .tab-title.active {  display: none; }

.banner-search .tab-title > a { display: none; }

.banner-search .tab-title:hover a,
.banner-search .tab-title.active a{ display: none; }

</style>

 <?php 

	global $concierge_option_data;


	if(isset($concierge_option_data['concierge-select-search-page'])){
		$search_page_id = $concierge_option_data['concierge-select-search-page'];										
	}
				

 ?>

<?php if(isset($concierge_option_data['concierge-home-page-search']) && !empty($concierge_option_data['concierge-home-page-search']) && isset($search_page_id) && !empty($search_page_id)) : ?>



	<?php 

		$args = array(
					'orderby'     => 'name', 
					'order'       => 'ASC',
					'fields'      => 'all', 
					
				);
				
		$args_vechicle_type = array(
					'orderby'     => 'name', 
					'order'       => 'ASC',
					'fields'      => 'all', 
					'exclude'    => '255',
				);
		
		if(taxonomy_exists('vechicle_type')){
			$terms = get_terms('vechicle_type', $args_vechicle_type);
			
		}


	 ?>		




<div class="banner-search">
  	<div class="container">
      	<div id="hero-tabs" class="banner-search-inner">
	        <ul class="custom-list tab-title-list clearfix">

	        	<?php if(isset($terms) && !empty($terms)) : ?>     

         
					<?php foreach($terms as $key => $value) { ?>
						<li class="tab-title showme"><a href="#<?php echo esc_attr($value->slug); ?>" data-rel="<?php echo esc_attr($value->slug); ?>"><?php echo esc_attr($value->name); ?></a></li>						
					<?php } ?>
	

				<?php endif; ?> 
			
	        </ul>

	        <input type="hidden" id="search-page-id" name="search_page_id" value="<?php echo esc_url(get_permalink($search_page_id)); ?> " >


	        <ul class="custom-list tab-content-list">


	        	<?php if(isset($terms) && !empty($terms)) : ?>     

         			<?php $count = 0; ?>	

					<?php foreach($terms as $key => $value) { ?>


						<!-- Start Yachts -->
						<li class="tab-content">


							<?php 
								
								$args_model = array(
								    'orderby'           => 'name', 
								    'order'             => 'ASC',						    
								    'fields'            => 'all', 						    
								);  
								
								/*
								if($key == 0){
									$model = $concierge_option_data['concierge_1stmodeltab_select'];
								}

								if($key == 1){
									$model = $concierge_option_data['concierge_2ndmodeltab_select'];
								}

								if($key == 2){
									$model = $concierge_option_data['concierge_3rdmodeltab_select'];
								}


								if($key == 3){
									$model = $concierge_option_data['concierge_4thmodeltab_select'];
								}
								*/
								//$count++;
								/*
								if(taxonomy_exists($model)){
									$model_terms = get_terms($model, $args_model);
								} */
								$model = 'private_jet_model';
								if(taxonomy_exists($model)){
									$model_terms = get_terms($model, $args_model);
								} 

								$args = array(
								    'orderby'           => 'name', 
								    'order'             => 'ASC',						    
								    'fields'            => 'all', 						    
								); 

								$locations = get_terms('vechicle_location', $args);

								$destinations = get_terms('vechicle_destination', $args);
								
								$departures = get_terms('vechicle_departure', $args);
								
								$persons = get_terms('vechicle_person', $args);
								
								$returntimes = get_terms('vechicle_returntime', $args);

							?>	
								



							<form method="POST" id="banner_search_form" action="<?php echo esc_url(get_permalink($search_page_id)); ?>" class="default-form home-page-fleet-search">
								
							
								<!--<!?php if(isset($concierge_option_data['concierge_show_search_location']) && !empty($concierge_option_data['concierge_show_search_location'])): ?>

								<span class="model select-box">
									<select id="locationparam" name="location" data-placeholder="From">
										<option value=""><!?php _e('From','concierge'); ?></option>
										<!?php if(isset($locations) && !empty($locations)): ?>
											
											<!?php foreach($locations as $location_key => $location_value){ ?>
												<option value="<!?php echo esc_attr($location_value->name); ?>"><!?php echo esc_attr($location_value->slug); ?></option>
											<!?php } ?>

										<!?php endif; ?>
									</select>
								</span>

								<!?php endif; ?>-->
							
								<?php if(isset($concierge_option_data['concierge_show_search_location']) && !empty($concierge_option_data['concierge_show_search_location'])): ?>

								<span class="model hire-input" title="From">
									<input type="text" id="locationparam" name="location" placeholder="From" >
									<?php endif; ?>
								</span>
								
								<!--<!?php if(isset($concierge_option_data['concierge_show_search_destination']) && !empty($concierge_option_data['concierge_show_search_destination'])): ?>-->
								
								<span class="model hire-input" title="To">
									<input id="destinationparam" type="text" name="destination" placeholder="To" >
								</span>
								
								<!--<!?php endif; ?>-->
								
								
								<?php if(isset($concierge_option_data['concierge_show_hireon']) && !empty($concierge_option_data['concierge_show_hireon'])): ?>
								<span class="hire-input calendar" title="Departure Date">								
									<input type="text" id="hireonparam" name="hireOn" placeholder="Depart On" data-dateformat="d-m-20y" >
									<i class="fa fa-calendar"></i>
								</span>
						

								<?php endif; ?>
								
								<!--<!?php if(isset($concierge_option_data['concierge_show_search_destination']) && !empty($concierge_option_data['concierge_show_search_destination'])): ?>-->
								
								<span class="model hire-input" title="Departure Time">
									<input id="time_of_departure" type="text" name="departure" placeholder="Depart Time" >
								</span>
								
								<!--<!?php endif; ?>-->
								
								<span id="testcheckbox" class="checkbox-input" title="Return">
								<p style="color:#fff;">Return  <input id="testcheckbox" type="checkbox" name="checkbox"></p>
								</span>
								
								<?php if(isset($concierge_option_data['concierge_show_returnon']) && !empty($concierge_option_data['concierge_show_returnon'])): ?>
										
								<span class="return-input calendar" title="Return Date">															
									<input id="testtextfield" type="text" class="returnoff" name="retunOn" placeholder="Return On" data-dateformat="d-m-20y" disabled="disabled">
									<i class="fa fa-calendar"></i>								
								</span>							

								<?php endif; ?>
								
								<!--<!?php if(isset($concierge_option_data['concierge_show_search_destination']) && !empty($concierge_option_data['concierge_show_search_destination'])): ?>-->
								
								<span class="return-input" title="Return Time">								
									<input id="time_of_return" class="time_of_returnoff" type="text" name="returntime" placeholder="Return Time" disabled="disabled">
								</span>
								
								<!--<!?php endif; ?>-->
								
								
								<!--<!?php if(isset($concierge_option_data['concierge_show_model']) && !empty($concierge_option_data['concierge_show_model'])): ?>

								<span class="model select-box">
									<select name="model" data-placeholder="Model">
										<option value=""><!?php _e('Model','concierge'); ?></option>
										<!?php if(isset($model_terms) && !empty($model_terms)): ?>
											
											<!?php foreach($model_terms as $model_key => $model_value){ ?>
												<option value="<!?php echo esc_attr($model_value->name); ?>"><!?php echo esc_attr($model_value->name); ?></option>
											<!?php } ?>

										<!?php endif; ?>
									</select>
								</span>

								<!?php endif; ?>-->
								
								<!--<!?php if(isset($concierge_option_data['concierge_show_search_person']) && !empty($concierge_option_data['concierge_show_search_person'])): ?>-->
								
								<span class="model hire-input" title="No. of Person">
									<input id="person_selected" type="text" name="person" placeholder="No. of Person" >
								</span>
								
								<!--<!?php endif; ?>-->


								<!--<input type="hidden" name="model_tax" value="<!?php echo esc_attr($model); ?>">-->
								<input type="hidden" name="location_tax" value="vechicle_location">
								<input type="hidden" name="destination_tax" value="vechicle_destination">
								<input type="hidden" name="departure_tax" value="vechicle_departure">
								<input type="hidden" name="returntime_tax" value="vechicle_returntime">
								<input type="hidden" name="person_tax" value="vechicle_person">
								<input type="hidden" name="vechicle_type_tax" value="vechicle_type">
 								
								
								<span class="submit-btn">
									<button type="submit" class="btn light" ><?php _e('Search','concierge'); ?></button>							
								</span>
								
								


							</form>
						</li>
			          	<!-- End Yachts -->




					<?php } ?>
	

				<?php endif; ?> 



	          	

	          	

	        </ul>
    	</div>
  	</div>
</div>

<?php endif; ?>