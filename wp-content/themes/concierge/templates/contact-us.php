<?php  
/**
 * Template Name: Contact-us
 *
 */
get_header();

?>


<?php 

	$company_address_info = get_post_meta(get_the_ID(), '_concierge_contact_address', true);

 ?>



<!-- Start Contact -->
<section class="contact-map style-2">

	<!-- Start Map Contact -->
	<div id="contact_us_map" style="width: 100%; height: 577px;"></div>
	<!-- End Map Contact -->

	<div class="container">

		<!-- Start Contact-Form -->
		<div class="col-lg-6 col-lg-offset-6 col-md-6 col-md-offset-6 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
			<div class="contact-form">
				<h5 class="contact-form-title"><?php _e('Send us a message','concierge'); ?></h5>
				<img src="<?php echo esc_url(CONCIERGE_IMAGE); ?>divisor2.png" alt="" class="divisor">
				<form id = "send_mail_to_office" class="default-form">
					<p class="alert-message warning"><i class="ico fa fa-exclamation-circle"></i> <?php _e('All fields are required!','conciege'); ?></p>
					<div class="row">

						<?php if(isset($company_address_info) && !empty($company_address_info)) : ?>

							<?php $agency_send_mail = array(); ?>
				
							<?php foreach($company_address_info as $key => $value) { ?>


								<?php foreach($value['_email'] as $email_key => $email_value){ ?>	
									<?php

										$agency_send_mail[$email_value] = $value['_location']; 
										
									?>
								<?php } ?>	


							<?php } ?>	

						<?php endif; ?>

							<?php _log($agency_send_mail); ?>

								<div class="col-lg-12">
									<p class="form-row">
										<span class="people select-box">
											<?php if(isset($agency_send_mail)) : ?>
											<select name="email_to" data-placeholder="">
												
												<?php foreach($agency_send_mail as $agency_key => $agency_value){ ?>
													<option value ="<?php echo esc_attr($agency_key); ?>"><?php echo esc_attr($agency_value); ?></option>
												<?php } ?>
												
											</select>
											<?php endif; ?>
										</span>
									</p>
								</div>
							

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p class="form-row">
								<input class="required" name="name" type="text" placeholder="Name">
							</p>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p class="form-row">
								<input class="required" name="phone" type="text" placeholder="Phone">
							</p>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p class="form-row">
								<input class="required" name="email" type="text" placeholder="Email">
							</p>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p class="form-row">
								<input class="required" name="topics" type="text" placeholder="Topic">
							</p>
						</div>
					</div>
					<p class="form-row">
						<textarea name="message" placeholder="How we can help?"></textarea>
					</p>
					<button class="btn light"><?php _e('Send Message','concierge'); ?></button>
					<div class="send-mail-success">
						<h5 class="success-msg"> <?php _e( 'Message send successfully !', 'concierge' ); ?></h5>
					</div>  

				</form>
			</div>
		</div>
		<!-- End Contact-Form -->
	</div>

</section>
<!-- End Contact -->



<!-- Start Locations -->
<section class="locations">
	<div class="container">
		<div class="row">

			<!-- Start Location-Agency -->
			<div class="location-agency">

				
				<?php if(isset($company_address_info) && !empty($company_address_info)) : ?>
				
					<?php foreach($company_address_info as $key => $value) { ?>

							
						<div class="col-lg-6 col-md-6">
							<h3 class="location-agency-title"><?php echo esc_attr($value['_location']); ?></h3>
							<h5 class="location-agency-subtitle"><?php _e('Office','concierge'); ?></h5>

							<ul class="custom-list location-agency-contact col-lg-6 col-md-6">
								<?php if(isset($value['_phone']) && is_array($value['_phone'])): ?>
								<?php foreach($value['_field'] as $field_key => $field_value){ ?>	
									<li><?php echo esc_attr($field_value); ?></li>
								<?php } ?>
								<?php endif; ?>

							</ul>
							
							<ul class="custom-list location-agency-contact col-lg-6 col-md-6">
								<?php _log(sizeof($value['_phone'])); ?>
								<?php if(isset($value['_phone']) && is_array($value['_phone'])): ?>	
									<?php foreach($value['_phone'] as $phone_key => $phone_value){ ?>	
									
									<li><?php if(!empty($phone_value)){ echo esc_attr('Phone: '); } ?><?php echo esc_attr($phone_value); ?></li>
								
									<?php } ?>
								<?php endif; ?>	
								<?php if(isset($value['_fax']) && is_array($value['_fax'])): ?>	
									<?php foreach($value['_fax'] as $fax_key => $fax_value){ ?>	
										<li><?php if(!empty($fax_value)){ echo esc_attr('Fax: '); } ?><?php echo esc_attr($fax_value); ?></li>
									<?php } ?>
								<?php endif; ?>						
							</ul>

							<ul class="custom-list location-agency-contact col-lg-6 col-md-6">
								<?php if(isset($value['_field2']) && is_array($value['_field2'])): ?>
									<?php foreach($value['_field2'] as $field_key => $field_value){ ?>	
										<li><?php echo esc_attr($field_value); ?></li>
									<?php } ?>
								<?php endif; ?>
							</ul>

							<ul class="custom-list location-agency-contact col-lg-6 col-md-6">
								<?php if(isset($value['_email']) && is_array($value['_email'])): ?>	
								<?php foreach($value['_email'] as $email_key => $email_value){ ?>	
									<li><?php if(!empty($email_value)){ echo esc_attr('Email: '); } ?><?php echo esc_attr($email_value); ?></li>
								<?php } ?>	
								<?php endif; ?>
								<?php if(isset($value['_website']) && is_array($value['_website'])): ?>
									<?php foreach($value['_website'] as $website_key => $website_value){ ?>	
										<li><?php if(!empty($website_value)){ echo esc_attr('Website: '); } ?><?php echo esc_attr($website_value); ?></li>
									<?php } ?>	
								<?php endif; ?>
							</ul>

						</div>
						
						<div class="col-lg-6 col-md-6">
							<?php 

						    	$image_id =  get_post_thumbnail_id( $agency_value->ID );
								$large_image = wp_get_attachment_image_src( $image_id ,'large'); 				

							?>
							<img src="<?php echo esc_url($large_image[0]); ?>" alt="" class="location-agency-thumbnail">
							
							
						</div>

					<?php } ?>	

				<?php endif; ?>


			<!-- End Location-Agency -->

		</div>
	</div>
</section>
<!-- End Locations -->

<!-- Start Locations -->
<section class="locations">
	<div class="container">
		<div class="row">


			<?php 

				$args = array(

						'post_type' => 'company_location',
						'show_per_page' => -1

					);

				$all_agencies = get_posts( $args );

				
			?>	

			<!-- Start Location-Agency -->

			<?php if(isset($all_agencies) && !empty($all_agencies)) :  ?>
				<?php foreach($all_agencies as $agecy_key => $agency_value){ ?>	

					<?php 

						$meta_value = get_post_meta($agency_value->ID,'_concierge_company_location_field',true); 
						
					 ?>

					<div class="location-agency">
						<div class="col-lg-6 col-md-6">
							<?php $agency_place = get_post_meta($agency_value->ID,'_concierge_company_location_place',true);  ?>
							<?php if(isset($agency_place) && !empty($agency_place)): ?>	
								<h3 class="location-agency-title"><?php echo esc_attr($agency_place); ?><?php _e('&nbsp;office','concierge'); ?></h3>
							<?php endif; ?>
							<h5 class="location-agency-subtitle"><?php _e('Office','concierge'); ?></h5>
							<ul class="custom-list location-agency-contact col-lg-6 col-md-6">
								
								<?php $agency_field = get_post_meta($agency_value->ID,'_concierge_company_location_field',true);  ?>
								<?php if(!empty($agency_field)): ?>
									<?php foreach($agency_field as $field_key => $field_value){ ?>	
										<li><?php echo esc_attr($field_value); ?></li>
									<?php } ?>
								<?php endif; ?>

							</ul>
							<ul class="custom-list location-agency-contact col-lg-6 col-md-6">
								
								<?php $agency_phone = get_post_meta($agency_value->ID,'_concierge_company_location_phone',true);  ?>
								<?php if(!empty($agency_phone)): ?>	
									<?php foreach($agency_phone as $phone_key => $phone_value){ ?>	
										<li><?php if(!empty($phone_value)){ echo esc_attr('Phone: '); } ?><?php echo esc_attr($phone_value); ?></li>
									<?php } ?>
								<?php endif; ?>

								<?php if(!empty($agency_fax)): ?>
								<?php $agency_fax = get_post_meta($agency_value->ID,'_concierge_company_location_fax',true);  ?>
									<?php foreach($agency_fax as $fax_key => $fax_value){ ?>	
										<li><?php if(!empty($fax_value)){ echo esc_attr('Fax: '); } ?><?php echo esc_attr($fax_value); ?></li>
									<?php } ?>	
								<?php endif; ?>

							</ul>
							<ul class="custom-list location-agency-contact col-lg-6 col-md-6">
								
								<?php $agency_email = get_post_meta($agency_value->ID,'_concierge_company_location_email',true);  ?>
								<?php if(!empty($agency_email)): ?>
									<?php foreach($agency_email as $email_key => $email_value){ ?>	
										<li><?php if(!empty($email_value)){ echo esc_attr('Email: '); } ?><?php echo esc_attr($email_value); ?></li>
									<?php } ?>	
								<?php endif; ?>
								
								<?php $agency_website = get_post_meta($agency_value->ID,'_concierge_company_location_website',true);  ?>
								<?php if(!empty($agency_website)): ?>
								<?php foreach($agency_website as $website_key => $website_value){ ?>	
									<li><?php if(!empty($website_value)){ echo esc_attr('Website: '); } ?><?php echo esc_attr($website_value); ?></li>
								<?php } ?>	
								<?php endif; ?>

							</ul>
						</div>
						<div class="col-lg-6 col-md-6">
							<?php 

						    	$image_id =  get_post_thumbnail_id( $agency_value->ID );
								$large_image = wp_get_attachment_image_src( $image_id ,'large'); 				

							?>
							<img src="<?php echo esc_url($large_image[0]); ?>" alt="" class="location-agency-thumbnail">
							
							
						</div>
					</div>

				<?php } ?>
			<?php endif; ?>
			<!-- End Location-Agency -->

		</div>
	</div>
</section>
<!-- End Locations -->

<!-- Start Partners -->
<section class="partners">
<?php get_template_part( 'templates/concierge', 'partner'); ?>  
</section>  
<!-- End Partners --> 



<?php get_footer();   ?>
	