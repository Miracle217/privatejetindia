<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'intercw172_jetin7');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
//define('AUTH_KEY',         'mINb6c9eCb5do8W3fKC5eIzN5BIqQWYWqxiYK49Pw4X9Crhv0vpn0ZQPR39NN0Sg');
//define('SECURE_AUTH_KEY',  'g89SqziFWqm8GPrNzTNAGlJSulHhr99c2we8NFwvdzObdbeiTcYcZAT3bK4t7Kah');
//define('LOGGED_IN_KEY',    'KU1gWE7fgnmP5V6DC1ZNsEomxPM5bZKOAox2hGApBLp2GI8EZIXAwkEurhmYa4aB');
//define('NONCE_KEY',        'nhvnRF1LBHgObueFtjq17qnZHifk9FNsG9QjWBHAPq66FTe0AwpfU4KlcW4WOr1y');
//define('AUTH_SALT',        '61u4iIOhbLv9yzGk2c3Tckao4pBXTNLjJUZqzUTa7zBpq7fXfgdXHo1O40apvACC');
//define('SECURE_AUTH_SALT', '6Mq7ThDkXfyrsjYQMg0i4pnqnPSh8IDaWYmPyBLrXLktt0EInSxJ4bE6Irxnvj3K');
//define('LOGGED_IN_SALT',   'QGJxwFwDgNV2sEYHa1qe4Eb1Z4YRE04oLULTRtnF7ld8bvQ6YjUZVVSTgflhxou4');
//define('NONCE_SALT',       'WI0ZPgnITOwaL0As1582zoSXPweV39GkafWwq1gpa1vrsnqsSH7lGnHDSK6ipijT');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
